package de.ur.iw.seeRaytracer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import javax.sound.midi.SysexMessage;

public class CellIntegerCoordinateBoundingBox {

    private CellIntegerCoordinate min;
    private CellIntegerCoordinate max;

    public CellIntegerCoordinateBoundingBox(CellIntegerCoordinate min, CellIntegerCoordinate max){
        this.min = min;
        this.max = max;
        checkIfMinLessThanMax();
    }

    private void checkIfMinLessThanMax(){
        int minX = min.getX1Component();
        int maxX = max.getX1Component();
        int minY = min.getX2Component();
        int maxY = max.getX2Component();
        int minZ = min.getX3Component();
        int maxZ = max.getX3Component();

        boolean worksCorrectly = true;
        worksCorrectly &= minX <= maxX;
        worksCorrectly &= minY <= maxY;
        worksCorrectly &= minZ <= maxZ;

        if(!worksCorrectly){
            System.out.println("Invalid CellIntegerBoundingBox Creation!");
            System.out.println(min.toString());
            System.out.println(max.toString());
        }
    }


    public CellIntegerCoordinate getMin(){
        return min;
    }

    public CellIntegerCoordinate getMax(){
        return max;
    }

    public double getRealEuclidianDiagonalLength(){
        double w = Cell.CELL_WIDTH;
        int deltaX = max.getX1Component() - min.getX1Component();
        deltaX *= w;
        int deltaY = max.getX2Component() - min.getX2Component();
        deltaY *= w;
        int deltaZ = max.getX3Component() - min.getX3Component();
        deltaZ *= w;
        return Math.sqrt(deltaX^2 + deltaY^2 + deltaZ^2);
    }

    public boolean isCellIntegerCoordinateInBoundingBox(CellIntegerCoordinate c){
        int x = c.getX1Component();
        int y = c.getX2Component();
        int z = c.getX3Component();
        int minX = min.getX1Component();
        int maxX = max.getX1Component();
        int minY = min.getX2Component();
        int maxY = max.getX2Component();
        int minZ = min.getX3Component();
        int maxZ = max.getX3Component();

        boolean result = true;

        result &= minX <= x;
        result &= maxX >= x;
        result &= minY <= y;
        result &= maxY >= y;
        result &= minZ <= z;
        result &= maxZ >= z;

        if(result == false){
            //System.out.println("(" + x + ", " + y + ", " + z + ") nicht in (" + minX + ", " + minY + ", " + minZ + ") bis (" + maxX + ", " + maxY + ", " + maxZ + ")");
        }


        return result;

    }

}

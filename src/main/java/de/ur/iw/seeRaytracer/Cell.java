package de.ur.iw.seeRaytracer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Cell {

    //TODO: changing this value cause bugs
    //longest triangle edge: 0.028717610399887527
    public static final double CELL_WIDTH = 0.03;
    private CellIntegerCoordinate coordinate;
    private Collection<Triangle> containedTriangles;


    public Cell(CellIntegerCoordinate coordinate){
        this.coordinate = coordinate;
        containedTriangles = new ArrayList<>();
    }

    public boolean isVectorPointInCell(Vector3D vector3D){
        /*boolean result = true;
        Vector3D cellOrigin = coordinate.toVectorPointInRealSpace();
        Vector3D cellLimitEnd = cellOrigin.add(new Vector3D(CELL_WIDTH, -CELL_WIDTH, CELL_WIDTH));
        result &= cellOrigin.getX() <= vector3D.getX();
        result &= vector3D.getX() < cellLimitEnd.getX();
        result &= cellOrigin.getY() >= vector3D.getY();
        result &= vector3D.getY() > cellLimitEnd.getY();
        result &= cellOrigin.getZ() <= vector3D.getZ();
        result &= vector3D.getZ() < cellLimitEnd.getZ();*/
        //TODO: implement
        var expectedCellPosition = CellIntegerCoordinate.createFromVectorPoint(vector3D);

        return coordinate.equals(expectedCellPosition);
    }

    public void addTriangle(Triangle triangle){
        containedTriangles.add(triangle);
    }


    public Collection<Triangle> getContainedTriangles(){
        return containedTriangles;
    }

    public static void main(String[] args){
        CellIntegerCoordinate cellCoord = new CellIntegerCoordinate(-1, -1, -1);
        Cell cell = new Cell(cellCoord);
        double smol = 0.0000001;
        double test = Cell.CELL_WIDTH-smol;
        Vector3D v = new Vector3D(test-smol, test-smol, test-smol);
        //v = cell.coordinate.toVectorPointInRealSpace();
        System.out.println(cell.isVectorPointInCell(v));
    }

}

package de.ur.iw.seeRaytracer;


import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class ClassThatHoldsAllCells {

    HashMap<CellIntegerCoordinate, Cell> allCells;
    CellIntegerCoordinateBoundingBox boundingBox;

    public ClassThatHoldsAllCells(Collection<Triangle> allTrianglesToBeDisplayed) {
        allCells = new HashMap<>();
        createNecessaryCellsAndPutContainedTrianglesIntoThem(allTrianglesToBeDisplayed);
        createIntegerCoordinateBoundingBoxFromTriangles(allTrianglesToBeDisplayed);
    }

    private void createIntegerCoordinateBoundingBoxFromTriangles(Collection<Triangle> triangles){
        //TODO: this is a terrible way to do this
        ArrayList<CellIntegerCoordinate> coordinates = new ArrayList<>();
        for(Triangle triangle : triangles){
            coordinates.addAll(getCollectionOfKeysThisTriangleBelongsTo(triangle));
        }
        CellIntegerCoordinate[] coordinateArray = new CellIntegerCoordinate[coordinates.size()];
        for(int i = 0; i < coordinateArray.length; i++) {
            coordinateArray[i] = coordinates.get(i);
        }
        boundingBox = CellIntegerCoordinate.getIntegerBoundingBoxForCoordinates(coordinateArray);
    }

    private void createNecessaryCellsAndPutContainedTrianglesIntoThem(Collection<Triangle> triangles) {
        for (Triangle triangle : triangles) {
            for (CellIntegerCoordinate coordinate : getCollectionOfKeysThisTriangleBelongsTo(triangle)) {
                allCells.putIfAbsent(coordinate, new Cell(coordinate));
                allCells.get(coordinate).addTriangle(triangle);
                //System.out.println(coordinate.toString());
            }
        }
    }

    private Collection<CellIntegerCoordinate> getCollectionOfKeysThisTriangleBelongsTo(Triangle triangle) {
        Collection<CellIntegerCoordinate> result = new ArrayList<>();

        CellIntegerCoordinateBoundingBox box = CellIntegerCoordinate.getIntegerBoundingBoxForTriangleIterator(triangle.iterator());
        int[] minComponents = box.getMin().getComponents();
        int[] maxComponents = box.getMax().getComponents();


        for (int x1 = minComponents[0]; x1 <= maxComponents[0]; x1++) {
            for (int x2 = minComponents[1]; x2 <= maxComponents[1]; x2++) {
                for (int x3 = minComponents[2]; x3 <= maxComponents[2]; x3++) {
                    result.add(new CellIntegerCoordinate(x1, x2, x3));
                }
            }
        }
        return result;
    }


    public List<Cell> getListOfCellsThatRayIntersects(Ray ray) {
        //TODO: fix
        ArrayList<Cell> result = new ArrayList<>();
        /*for(Cell cell : allCells.values()){
            if(Math.random() > 0.5){
                result.add(cell);
            }
        }*/


        //camera - object space bounding box
        //TODO: make this 1 array and 1 method call
        CellIntegerCoordinate[] mins = new CellIntegerCoordinate[]{CellIntegerCoordinate.createFromVectorPoint(ray.getOrigin()), boundingBox.getMin()};
        CellIntegerCoordinate[] maxs = new CellIntegerCoordinate[]{CellIntegerCoordinate.createFromVectorPoint(ray.getOrigin()), boundingBox.getMax()};
        CellIntegerCoordinateBoundingBox boxMin = CellIntegerCoordinate.getIntegerBoundingBoxForCoordinates(mins);
        CellIntegerCoordinateBoundingBox boxMax = CellIntegerCoordinate.getIntegerBoundingBoxForCoordinates(maxs);
        CellIntegerCoordinateBoundingBox cameraObjectSpaceBoundingBox = new CellIntegerCoordinateBoundingBox(boxMin.getMin(), boxMax.getMax());
        for (int value : CellIntegerCoordinate.createFromVectorPoint(ray.getOrigin()).getComponents()){
            //System.out.print(value + ", ");
        }
        //System.out.println("");



        //source: http://www.cse.yorku.ca/~amana/research/grid.pdf
        //source2: https://github.com/francisengelmann/fast_voxel_traversal/blob/master/main.cpp
        double _bin_size = Cell.CELL_WIDTH;
        Vector3D ray_end = ray.getOrigin().add(cameraObjectSpaceBoundingBox.getRealEuclidianDiagonalLength() * 1.01, ray.getNormalizedDirection());
        ArrayList<Cell> visited_voxels = new ArrayList<>();

        CellIntegerCoordinate current_voxel = new CellIntegerCoordinate((int)Math.floor(ray.getOrigin().getX() / _bin_size),
                (int)Math.floor(ray.getOrigin().getY() / _bin_size), (int)Math.floor(ray.getOrigin().getZ() / _bin_size));
        CellIntegerCoordinate last_voxel = new CellIntegerCoordinate((int)Math.floor(ray_end.getX() / _bin_size),
                (int)Math.floor(ray_end.getY() / _bin_size), (int)Math.floor(ray_end.getZ() / _bin_size));



        double stepX = Math.signum(ray.getNormalizedDirection().getX());
        double stepY = Math.signum(ray.getNormalizedDirection().getY());
        double stepZ = Math.signum(ray.getNormalizedDirection().getZ());

        double next_voxel_boundary_x = (current_voxel.getX1Component()+stepX)*_bin_size;
        double next_voxel_boundary_y = (current_voxel.getX2Component()+stepY)*_bin_size;
        double next_voxel_boundary_z = (current_voxel.getX3Component()+stepZ)*_bin_size;

        double tMaxX = (ray.getNormalizedDirection().getX()!=0) ? ((next_voxel_boundary_x - ray.getOrigin().getX())/ray.getNormalizedDirection().getX()) : Double.MAX_VALUE;
        double tMaxY = (ray.getNormalizedDirection().getY()!=0) ? ((next_voxel_boundary_y - ray.getOrigin().getY())/ray.getNormalizedDirection().getY()) : Double.MAX_VALUE;
        double tMaxZ = (ray.getNormalizedDirection().getZ()!=0) ? ((next_voxel_boundary_z - ray.getOrigin().getZ())/ray.getNormalizedDirection().getZ()) : Double.MAX_VALUE;

        double tDeltaX = (ray.getNormalizedDirection().getX()!=0) ? (_bin_size/ray.getNormalizedDirection().getX()*stepX) : Double.MAX_VALUE;
        double tDeltaY = (ray.getNormalizedDirection().getY()!=0) ? (_bin_size/ray.getNormalizedDirection().getY()*stepY) : Double.MAX_VALUE;
        double tDeltaZ = (ray.getNormalizedDirection().getZ()!=0) ? (_bin_size/ray.getNormalizedDirection().getZ()*stepZ) : Double.MAX_VALUE;

        int[] diff = new int[]{0, 0, 0};
        boolean neg_ray=false;
        if (current_voxel.getX1Component()!=last_voxel.getX1Component() && ray.getNormalizedDirection().getX()<0) { diff[0]=-1; neg_ray=true;}
        if (current_voxel.getX2Component()!=last_voxel.getX2Component() && ray.getNormalizedDirection().getY()<0) { diff[1]=-1; neg_ray=true;}
        if (current_voxel.getX3Component()!=last_voxel.getX3Component() && ray.getNormalizedDirection().getZ()<0) {diff[2]=-1; neg_ray=true;}

        if(allCells.get(current_voxel) != null){
            visited_voxels.add( allCells.get(current_voxel));
        }
        if(neg_ray){
            current_voxel = new CellIntegerCoordinate(current_voxel.getX1Component()+diff[0], current_voxel.getX2Component()+diff[1], current_voxel.getX3Component()+diff[2]);

            if(allCells.get(current_voxel) != null){
                visited_voxels.add( allCells.get(current_voxel));
            }
        }


        //int iteration_count = 0;
        while(/*last_voxel.equals(current_voxel) == false*/cameraObjectSpaceBoundingBox.isCellIntegerCoordinateInBoundingBox(current_voxel)){
            //System.out.println(iteration_count++);
            if(tMaxX < tMaxY){
                if(tMaxX < tMaxZ){
                    current_voxel = new CellIntegerCoordinate((int) (current_voxel.getX1Component()+stepX), current_voxel.getX2Component(), current_voxel.getX3Component());
                    tMaxX += tDeltaX;
                } else {
                    current_voxel = new CellIntegerCoordinate(current_voxel.getX1Component(), current_voxel.getX2Component(), (int) (current_voxel.getX3Component()+stepZ));
                    tMaxZ += tDeltaZ;
                }
            } else {
                if(tMaxY < tMaxZ){
                    current_voxel = new CellIntegerCoordinate(current_voxel.getX1Component(), (int) (current_voxel.getX2Component()+stepY), current_voxel.getX3Component());
                    tMaxY+=tDeltaY;
                } else {
                    current_voxel = new CellIntegerCoordinate(current_voxel.getX1Component(), current_voxel.getX2Component(), (int) (current_voxel.getX3Component()+stepZ));
                    tMaxZ += tDeltaZ;
                }
            }
            if(allCells.get(current_voxel) != null){
                visited_voxels.add(allCells.get(current_voxel));
            }
        }
        //System.out.print("returned visited voxels");

        return visited_voxels;
    }


}

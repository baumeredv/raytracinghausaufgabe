package de.ur.iw.seeRaytracer;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Scene {

    private ClassThatHoldsAllCells cells;

    /**
     * Adds triangles to this scene.
     */
    public void addAll(Collection<Triangle> triangles) {
        cells = new ClassThatHoldsAllCells(triangles);
    }

    /**
     * Computes the color of the light that is seen when looking at the scene along the given ray.
     */
    public Color computeLightThatFlowsBackAlongRay(Ray ray) {
        // determine which part of the scene gets hit by the given ray, if any
        SurfaceInformation closestSurface = findFirstIntersection(ray);

        if (closestSurface != null) {
            // return surface color at intersection point
            return closestSurface.computeEmittedLightInGivenDirection(ray.getNormalizedDirection().negate());
        } else {
            // nothing hit -> return transparent
            return new Color(0, 0, 0, 0);
        }
    }

    /**
     * Traces the given ray through the scene until it intersects anything.
     *
     * @param ray describes the exact path through the scene that will be searched.
     * @return information on the surface point where the first intersection of the ray with any scene object occurs - or null for no intersection.
     */
    private SurfaceInformation findFirstIntersection(Ray ray) {
        boolean smartRender = false;

        if (!smartRender) {
            SurfaceInformation closestSurface = null;
            double distanceToClosestSurface = Double.POSITIVE_INFINITY;

            var reasonableCells = cells.getListOfCellsThatRayIntersects(ray);
            ArrayList<Triangle> triangles = new ArrayList<>();
            for (Cell reasonableCell : reasonableCells) {
                triangles.addAll(reasonableCell.getContainedTriangles());
            }


            for (var triangle : triangles) {
                var surface = triangle.intersectWith(ray);
                if (surface != null) {
                    double distanceToSurface = surface.getPosition().distance(ray.getOrigin());
                    if (distanceToSurface < distanceToClosestSurface) {
                        distanceToClosestSurface = distanceToSurface;
                        closestSurface = surface;
                    }
                }
            }
            return closestSurface;
        }
        List<Cell> reasonableCells = cells.getListOfCellsThatRayIntersects(ray);
        SurfaceInformation closestIntersection = null;
        double distanceToClosestIntersection = Double.POSITIVE_INFINITY;

        //boolean stopCheckingAfterTheNextCell = false;
        for (Cell cell : reasonableCells) {
            for (Triangle triangle : cell.getContainedTriangles()) {
                SurfaceInformation intersection = triangle.intersectWith(ray);

                if (intersection != null) {
                    if (cell.isVectorPointInCell(intersection.getPosition())) {
                        //System.out.println("do I get here");
                        double distanceToIntersection = intersection.getPosition().distance(ray.getOrigin());
                        if (distanceToIntersection < distanceToClosestIntersection) {
                            distanceToClosestIntersection = distanceToIntersection;
                            closestIntersection = intersection;
                        }
                    }
                }
            }
            if (closestIntersection != null) {
                //if(stopCheckingAfterTheNextCell == true){
                return closestIntersection;
                //}
                //stopCheckingAfterTheNextCell = true;
            }
        }

        return closestIntersection;

    }
}

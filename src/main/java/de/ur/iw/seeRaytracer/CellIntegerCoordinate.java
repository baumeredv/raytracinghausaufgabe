package de.ur.iw.seeRaytracer;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import java.util.ArrayList;
import java.util.Iterator;

public class CellIntegerCoordinate {


    private int x1Component;
    private int x2Component;
    private int x3Component;

    public CellIntegerCoordinate(int x1Component, int x2Component, int x3Component) {
        if(x1Component == -0){
            x1Component = 0;
        }
        if(x2Component == -0){
            x2Component = 0;
        }
        if(x3Component == -0){
            x3Component = 0;
        }
        this.x1Component = x1Component;
        this.x2Component = x2Component;
        this.x3Component = x3Component;
    }

    @Override
    public boolean equals(Object o){
        if (this == o){
            return true;
        }
        if(o == null){
            return false;
        }
        if(getClass() != o.getClass()){
            return false;
        }
        CellIntegerCoordinate c = (CellIntegerCoordinate) o;
        return c.getX1Component() == getX1Component() && c.getX2Component() == getX2Component() && c.getX3Component() == getX3Component();

    }

    @Override
    public int hashCode(){
        return 137*x1Component+149*x2Component+163*x3Component;
    }

    public String toString(){
        return "(" + x1Component + ", " + x2Component + ", " + x3Component + ")";
    }

    public int getX1Component() {
        return this.x1Component;
    }

    public int getX2Component() {
        return this.x2Component;
    }

    public int getX3Component() {
        return this.x3Component;
    }

    public Vector3D toVectorPointInRealSpace() {
        double width = Cell.CELL_WIDTH;
        return new Vector3D(x1Component * width, x2Component * width, x3Component * width);
    }

    public int[] getComponents() {
        return new int[]{getX1Component(), getX2Component(), getX3Component()};
    }

    public static CellIntegerCoordinate createFromVectorPoint(Vector3D vector3D) {
        int x1 = (int) Math.floor(vector3D.getX() / Cell.CELL_WIDTH);
        int x2 = (int) Math.floor(vector3D.getY() / Cell.CELL_WIDTH);
        int x3 = (int) Math.floor(vector3D.getZ() / Cell.CELL_WIDTH);
        return new CellIntegerCoordinate(x1, x2, x3);
    }

    public static CellIntegerCoordinate createFromArray(int[] array){
        assert(array.length == 3);
        int x1 = array[0];
        int x2 = array[1];
        int x3 = array[2];
        return new CellIntegerCoordinate(x1, x2, x3);
    }

    public static CellIntegerCoordinateBoundingBox getIntegerBoundingBoxForTriangleIterator(Iterator<Vector3D> iterator){
        ArrayList<CellIntegerCoordinate> vertexIntegerCoordinates = new ArrayList<>();
        for (Iterator<Vector3D> it = iterator; it.hasNext(); ) {
            Vector3D vertex = it.next();
            vertexIntegerCoordinates.add(CellIntegerCoordinate.createFromVectorPoint(vertex));
        }
        CellIntegerCoordinate[] coordinates = new CellIntegerCoordinate[vertexIntegerCoordinates.size()];
        for(int i = 0; i < coordinates.length; i++){
            coordinates[i] = vertexIntegerCoordinates.get(i);
        }
        return getIntegerBoundingBoxForCoordinates(coordinates);
    }

    public static CellIntegerCoordinateBoundingBox getIntegerBoundingBoxForCoordinates(CellIntegerCoordinate[] coordinates) {
        int[] minComponents = new int[]{Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE};
        int[] maxComponents = new int[]{Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE};


        for (int k = 0; k < coordinates.length; k++) {
            int[] coordinate1Components = coordinates[k].getComponents();
            for (int i = 0; i < 3; i++) {
                minComponents[i] = Math.min(minComponents[i], coordinate1Components[i]);
                maxComponents[i] = Math.max(maxComponents[i], coordinate1Components[i]);
            }
        }

        return new CellIntegerCoordinateBoundingBox(CellIntegerCoordinate.createFromArray(minComponents), CellIntegerCoordinate.createFromArray(maxComponents));
    }


}
